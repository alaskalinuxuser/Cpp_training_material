# Cpp_training_material

A directory of my work while participating in a class about learning how to program in C++.
All of my work is licensed under the Apache2.0, however, some of the class artwork and sounds for the Timber! game may not be opensourced.

All of the artwork and sounds for the JelloStorm! game are open source, under the Apache2.0 license.

To build these games, you need libsfml installed on your Linux system. Then, in each game folder, run the ./build.sh file to build the source.
